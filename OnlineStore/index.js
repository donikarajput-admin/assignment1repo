const express = require ('express');
const path = require ('path');
const {check, validationResult} = require('express-validator');

var myApp = express ();
myApp.use (express. urlencoded({extended:true}));


myApp.set ('views', path.join (__dirname, 'views'));
myApp.use (express.static(__dirname + '/public'));
myApp.use( express.static( "public" ));
myApp.set ('view engine', 'ejs');


//home page 
myApp.get ('/', function (req, res)
{
    res.render('form'); 
});

myApp.post('/', [
    check ('name', 'Name is required!').notEmpty(),
    check ('address', 'Address is required!').notEmpty(),
    check ('city', 'City is required!').notEmpty(),
    check ('email', 'Email is required!').isEmail(),
    check ('phone', 'Phone is required!').isMobilePhone(),
    check ('province', 'Province is required!').notEmpty()
], function (req,res){
    const errors = validationResult(req);
    console.log(errors);
    if (!errors.isEmpty())
    {
        res.render('form', {
            errors : errors.array()
        });
    }
    else 
    {
        var name = req.body.name;
        var email = req.body.email;
        var phone = req.body.phone;
        var address = req.body.address;
        var city = req.body.city;
        var province = req.body.province;
        var tshirt = req.body.tshirt;
        var jeans = req.body.jeans;
        var top = req.body.top;
        var jacket = req.body.jacket;
        var hoodie = req.body.hoodie;

        var tshirtPrice = 5 , jeansPrice = 20, topPrice = 4, jacketPrice = 100,hoodiePrice =70,subTotal = 0, displayReceipt;
        if(tshirt > 0){
            var tshirtTotal = tshirtPrice * tshirt;
            subTotal += tshirtTotal;
        }
        if(jeans > 0){
            var jeansTotal = jeansPrice * jeans;
            subTotal += jeansTotal;
        }
        if(top > 0){
            var topTotal = topPrice * top;
            subTotal += topTotal;
        }
        if(jacket > 0){
            var jacketTotal = jacketPrice * jacket;
            subTotal += jacketTotal;
        }
        if(hoodie > 0){
            var hoodieTotal = hoodiePrice * hoodie;
            subTotal += hoodieTotal;
        }
        
        var tax = 0,taxPer = 0;
        if(province == 'Ontario'){
            taxPer = 0.13;
            tax = subTotal * taxPer;
        }else if(province == 'Quebec'){
            taxPer = 0.9;
            tax = subTotal * taxPer;
        }else if(province == 'Alberta'){
            taxPer = 0.5;
            tax = subTotal * taxPer;
        }
        var total = subTotal + tax;

        if(subTotal < 10){
            displayReceipt = 0;
            var receiptMessage = 'Minimum purchase should be of $10.';
            console.log(displayReceipt + ' ' + receiptMessage);
        }

        var pageData = {
            name : name,
            email : email,
            phone : phone,
            address : address,
            city : city,
            province : province,
            tshirt : tshirt,
            tshirtTotal : tshirtTotal,
            jeans : jeans,
            jeansTotal : jeansTotal,
            top : top,
            topTotal : topTotal,
            jacket : jacket,
            jacketTotal : jacketTotal,
            hoodie : hoodie,
            hoodieTotal : hoodieTotal,
            subTotal : subTotal,
            tax : tax,
            taxPer : taxPer,
            total : total,
            displayReceipt : displayReceipt,
            receiptMessage : receiptMessage
        }

        res.render ('form', pageData);
    }
});


myApp.get('/author', function (req,res){
    res.render('author',{
        name : "Donika Rajput",
        studentNumber : '8749000'
    });
});

myApp.listen(8080); 
console.log('Website opened at port 8080!');